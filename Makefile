install:
	[ ! -f trf.map.gz ] || rm -f trf.map.gz
	gzip -k trf.map
	install trf.map.gz $(DESTDIR)/usr/share/kbd/keymaps/i386/fgGIod/trf.map.gz || true
	rm -f trf.map.gz
	install tr $(DESTDIR)//usr/share/X11/xkb/symbols/tr
